import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  @Input() user: User;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getUser();
    console.log("/// user", this.user);
  }

  getUser(): void {
    const uuid = this.route.snapshot.paramMap.get('UUID');
    this.userService.getUser(uuid)
      .subscribe(user => this.user = user);
  }

  goBack(): void {
    this.location.back();
  }

}
