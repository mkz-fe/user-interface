import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { User, Quote } from '../user';
import { UserService } from '../user.service';
import { UserDetailsComponent } from '../user-details/user-details.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  private users: User[];
  selectedUser: User;
  private elementColors = ['#f2849e', '#7ecaf6', '#7bd0c1', '#c75b9b', '#ae85ca', '#8499e7'];
  private isLoading: boolean;
  private usersUrl = 'https://randomname.de/?format=json&count=1&images=1';
  private quotesURL = 'http://quotesondesign.com/api/3.0/api-3.0.json';
  private numberOfUsers = 12;

  constructor(private userService: UserService) {
    this.users = new Array();
  }

  ngOnInit() {
    this.getUsers();
  }

  onSelect(user: User): void {
    this.selectedUser = user;

  }

  getUsers(): void {
    this.isLoading = true;
    this.userService.getUsers(this.usersUrl).subscribe(data =>{
      this.users = data;
      this.isLoading = false;
    });
  }

  getBackgroundColor(i: number): string {

    for (let j=this.elementColors.length; j>0; j--) {
      if (i%j==0) {
        return this.elementColors[j-1];
      }
    }
  }

}
