import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
//import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User, Quote } from './user';
import { USERS} from './mock-users';

@Injectable()
export class UserService {

  users = USERS;
  timer: any;

  constructor(/*private http: HttpClient*/) {
  }

  getUsers(url: string): Observable<User[]>{
    return of(this.users);
  }

  getUser(uuid: string): Observable<User>{
    return of(this.users.find(user => user.UUID==uuid));
  }

}
