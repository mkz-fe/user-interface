import { User } from './user';

export const USERS: User[] = [
    {
        "firstName": "Gabriela",
        "lastName": "Deininger",
        "gender": "f",
        "birthday": "12.03.1983",
        "email": "Deininger.Gabriela@freundin.ru",
        "age": 34,
        "password": "Ahr9biChe,",
        "username": "Elfa",
        "location": {
            "zip": "53506",
            "city": "Lind",
            "street": {
                "name": "Zur Wolfsmühle",
                "number": 75
            }
        },
        "UUID": "CC1F394F-C0E0-4460-82B8-3F2DF8737510",
        "image": "https://randomname.de/images/twitter_kristigrassi.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Gregor",
        "lastName": "Roth",
        "gender": "m",
        "birthday": "27.03.1984",
        "email": "RothGregor@fast-mail.fr",
        "age": 33,
        "password": "Aiv3xohth+",
        "username": "Avaneg",
        "location": {
            "zip": "39517",
            "city": "Jerchel",
            "street": {
                "name": "Zwischen beiden Sielen",
                "number": 27
            }
        },
        "UUID": "29B2ADFD-3699-4051-B74D-A3B82AD5AC8B",
        "image": "https://randomname.de/images/twitter_romainall.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Rosel",
        "lastName": "Vollrath",
        "gender": "f",
        "birthday": "09.05.1987",
        "email": "RoselVollrath@spambog.com",
        "age": 30,
        "password": "Do@e0roch&",
        "username": "Tipnul",
        "location": {
            "zip": "63639",
            "city": "Flörsbachtal",
            "street": {
                "name": "Knopper Weg",
                "number": 30
            }
        },
        "UUID": "E8E58AF1-28F2-478D-995A-C5475B220B31",
        "image": "https://randomname.de/images/twitter_monicaczarny.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Reni",
        "lastName": "Imhof",
        "gender": "f",
        "birthday": "09.04.1992",
        "email": "Reni_Imhof@discardmail.com",
        "age": 25,
        "password": "eeri3Wei*g",
        "username": "Farfim",
        "location": {
            "zip": "17089",
            "city": "Breest",
            "street": {
                "name": "Brünndler Weg",
                "number": 44
            }
        },
        "UUID": "1B93D2A6-1306-4C49-A4B0-040D80D38B43",
        "image": "https://randomname.de/images/twitter_giuliusa.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Hansheinz",
        "lastName": "Hauke",
        "gender": "m",
        "birthday": "01.12.1993",
        "email": "HaukeHansheinz@0815.su",
        "age": 24,
        "password": "aib#ohlo4E",
        "username": "Maniald93",
        "location": {
            "zip": "93186",
            "city": "Pettendorf",
            "street": {
                "name": "Zehentbauernstraße",
                "number": 24
            }
        },
        "UUID": "0285F0A9-F4DA-4613-A888-6E7FEA36B848",
        "image": "https://randomname.de/images/twitter_straykov.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Eunike",
        "lastName": "Brucker",
        "gender": "f",
        "birthday": "18.12.1994",
        "email": "EunikeBrucker@instantmail.fr",
        "age": 23,
        "password": "iNg0shogh-",
        "username": "Imrilift",
        "location": {
            "zip": "24211",
            "city": "Preetz",
            "street": {
                "name": "Solgasse",
                "number": 62
            }
        },
        "UUID": "1A42183C-1D8D-4BB4-9D2D-BDBED07C6778",
        "image": "https://randomname.de/images/twitter_seobaby.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Maxi",
        "lastName": "Stender",
        "gender": "f",
        "birthday": "11.02.1989",
        "email": "Maxi.Stender@s0ny.net",
        "age": 29,
        "password": "je=Z7Ob1ri",
        "username": "Hwemnor",
        "location": {
            "zip": "46535",
            "city": "Dinslaken",
            "street": {
                "name": "Reusaer Straße",
                "number": 92
            }
        },
        "UUID": "ED5E9F12-7709-42C8-B0A8-CB2F8FF19A62",
        "image": "https://randomname.de/images/twitter_courtbrillhart.jpg",
        "glasses": "yes",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Hans-Karl",
        "lastName": "Kolodziej",
        "gender": "m",
        "birthday": "27.02.2001",
        "email": "Hans-KarlKolodziej@used-product.fr",
        "age": 16,
        "password": "wu,ghu0Xef",
        "username": "Meraec",
        "location": {
            "zip": "98634",
            "city": "Melpers",
            "street": {
                "name": "Bickefeldstraße",
                "number": 57
            }
        },
        "UUID": "C2FC5D90-E7CF-4677-8AB6-F75811CF2800",
        "image": "https://randomname.de/images/twitter_polvova.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Nelly",
        "lastName": "Brandl",
        "gender": "f",
        "birthday": "24.02.1984",
        "email": "BrandlNelly@zaktouni.fr",
        "age": 33,
        "password": "aiX@i6aal3",
        "username": "Methenelar",
        "location": {
            "zip": "06567",
            "city": "Rottleben",
            "street": {
                "name": "Peter-Krall-Straße",
                "number": 25
            }
        },
        "UUID": "B5408DBF-1898-41E1-B16A-EC9BB7D5E152",
        "image": "https://randomname.de/images/twitter_catkazmir.jpg",
        "glasses": "yes",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Erdfried",
        "lastName": "Vögele",
        "gender": "m",
        "birthday": "04.03.1980",
        "email": "Voegele-Erdfried80@cyber-innovation.club",
        "age": 37,
        "password": "gook9Loo#S",
        "username": "Greyvald80",
        "location": {
            "zip": "98746",
            "city": "Meuselbach-Schwarzmühle",
            "street": {
                "name": "Springmannskamp",
                "number": 35
            }
        },
        "UUID": "64443D6A-3AAB-401C-ADE1-37FDE7EAAF96",
        "image": "https://randomname.de/images/twitter_alexandermayes.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Gundel",
        "lastName": "Wege",
        "gender": "f",
        "birthday": "13.04.1990",
        "email": "Gundel_Wege@discardmail.de",
        "age": 27,
        "password": "Quoo1chie~",
        "username": "Parathelon90",
        "location": {
            "zip": "65558",
            "city": "Hirschberg",
            "street": {
                "name": "Rolander Weg",
                "number": 29
            }
        },
        "UUID": "A73B6328-58DE-48FC-A1C7-BA5F065A7BD4",
        "image": "https://randomname.de/images/twitter_sam_potts.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Henri",
        "lastName": "Schüller",
        "gender": "f",
        "birthday": "06.02.1985",
        "email": "HenriSchueller@tempr.email",
        "age": 33,
        "password": "Ta-od~ei4v",
        "username": "Plesyrre",
        "location": {
            "zip": "25782",
            "city": "Hövede",
            "street": {
                "name": "Am Strahlberg",
                "number": 33
            }
        },
        "UUID": "028CEA61-B64D-4CB9-ABF6-A8966DF12B30",
        "image": "https://randomname.de/images/twitter_kaniasty.jpg",
        "glasses": "yes",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Klaus Peter",
        "lastName": "Moritz",
        "gender": "m",
        "birthday": "16.06.1987",
        "email": "Moritz-Klaus Peter@pfui.ru",
        "age": 30,
        "password": "quah/b2Eih",
        "username": "Julureleb87",
        "location": {
            "zip": "39365",
            "city": "Wormsdorf",
            "street": {
                "name": "Feldbrandstraße",
                "number": 71
            }
        },
        "UUID": "87839313-AFFB-462E-9EA4-E2293EE5398B",
        "image": "https://randomname.de/images/twitter_humanhair.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Timon",
        "lastName": "Pape",
        "gender": "m",
        "birthday": "25.09.1981",
        "email": "Pape.Timon@s-s.igg.biz",
        "age": 36,
        "password": "EeSi2Cai)h",
        "username": "Trisnach81",
        "location": {
            "zip": "99869",
            "city": "Mühlberg",
            "street": {
                "name": "Perlesöd",
                "number": 83
            }
        },
        "UUID": "9F3595A2-952A-43F9-ACA6-E770AD5B0227",
        "image": "https://randomname.de/images/twitter_florianmascaro.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Elise",
        "lastName": "Stöver",
        "gender": "f",
        "birthday": "21.03.1984",
        "email": "Elise.Stoever@klick-tipp.us",
        "age": 33,
        "password": "iewee0vo$V",
        "username": "Stavegill",
        "location": {
            "zip": "92655",
            "city": "Grafenwöhr",
            "street": {
                "name": "Poxdorf",
                "number": 93
            }
        },
        "UUID": "0DA7C9EF-5318-46CA-B06F-E6AA3AA065B3",
        "image": "https://randomname.de/images/twitter_kaniasty.jpg",
        "glasses": "yes",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Alex",
        "lastName": "Stegmann",
        "gender": "m",
        "birthday": "05.07.1996",
        "email": "Alex-Stegmann@cyber-innovation.club",
        "age": 21,
        "password": "voh!h3uuHi",
        "username": "Karalrem",
        "location": {
            "zip": "67125",
            "city": "Dannstadt-Schauernheim",
            "street": {
                "name": "Kellenhusener Weg",
                "number": 47
            }
        },
        "UUID": "1A1DCE2B-68E1-4157-AF28-48DD68CBB27C",
        "image": "https://randomname.de/images/twitter_itayhaephrati.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Brunhilde",
        "lastName": "Tewes",
        "gender": "f",
        "birthday": "30.06.1986",
        "email": "BrunhildeTewes@u14269.ml",
        "age": 31,
        "password": "eich#it7Oo",
        "username": "Ealenan86",
        "location": {
            "zip": "66953",
            "city": "Pirmasens",
            "street": {
                "name": "Meschederstraße",
                "number": 73
            }
        },
        "UUID": "CA496CCC-0F6A-4923-9C0F-1D62BA53BF5F",
        "image": "https://randomname.de/images/twitter_latoyale.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Elsabe",
        "lastName": "Patzelt",
        "gender": "f",
        "birthday": "30.12.1979",
        "email": "Elsabe-Patzelt@disign-revelation.com",
        "age": 38,
        "password": "cee~X7wae5",
        "username": "Sowbald79",
        "location": {
            "zip": "39291",
            "city": "Magdeburgerforth",
            "street": {
                "name": "Am Finkenbach",
                "number": 71
            }
        },
        "UUID": "EC1FE314-515F-49CE-B42E-208BA751FCF3",
        "image": "https://randomname.de/images/twitter_raqueldesigns.jpg",
        "glasses": "yes",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Edgar",
        "lastName": "Weimann",
        "gender": "m",
        "birthday": "07.03.1993",
        "email": "WeimannEdgar93@mail-easy.fr",
        "age": 24,
        "password": "ao-vaiZai1",
        "username": "Glutred",
        "location": {
            "zip": "58708",
            "city": "Menden",
            "street": {
                "name": "Kuchenäcker",
                "number": 18
            }
        },
        "UUID": "8DDA595D-6324-4598-827D-373A00E284B2",
        "image": "https://randomname.de/images/twitter_lukisjama.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    },
    {
        "firstName": "Bernhild",
        "lastName": "Popp",
        "gender": "f",
        "birthday": "09.02.1988",
        "email": "Popp.Bernhild@s-s.nut.cc",
        "age": 30,
        "password": "Ieng*ee3qu",
        "username": "Camaskelor88",
        "location": {
            "zip": "09120",
            "city": "Chemnitz",
            "street": {
                "name": "Schkeuditzer Straße",
                "number": 40
            }
        },
        "UUID": "ACB053B8-97A9-425E-8766-E08D3615F65E",
        "image": "https://randomname.de/images/twitter_txideagal.jpg",
        "glasses": "no",
        "quote": "Every designers' dirty little secret is that they copy other designers' work. They see work they like, and they imitate it. Rather cheekily, they call this inspiration."
    }
];
