export interface User {
  UUID: string;
  age: number;
  birthday: string;
  email: string;
  firstName: string;
  gender: string;
  glasses: string;
  image: string;
  lastName: string;
  location: {};
  password: string;
  username: string;
  quote: string;
}

export interface Quote {
  id: number;
  quote: string;
  author: string;
  permalink: string;
}
